export class User {
	loginEmail: string;
	userName: string;
	role: string;
	token?: string;
}