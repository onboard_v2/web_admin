import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';

import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { EmailVerifyComponent } from './components/email-verify/email-verify.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { EmployersComponent } from './components/employers/employers.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CertificatesComponent } from './components/certificates/certificates.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { NewEmployeesComponent } from './components/employees/new-employees/new-employees.component';
import { EditEmployeesComponent } from './components/employees/edit-employees/edit-employees.component';
import { EditEmployerComponent } from './components/employers/edit-employer/edit-employer.component';
import { JobEmployeesComponent } from './components/jobs/job-employees/job-employees.component';


import { AuthGuard } from './helpers/auth.gaurd';

const routes: Routes = [
	{ path: '', component: LoginComponent, canActivate: [AuthGuard] },
	{ path: 'login', component: LoginComponent },
	// { path: 'register', component: SignupComponent },
	{ path: 'forget-password', component: ForgetPasswordComponent },
	{ path: 'reset/:passwordToken', component: ResetPasswordComponent },
	{ path: 'change-password/:id', component: ChangePasswordComponent, canActivate: [AuthGuard] },
	{ path: 'emailConfirm/:emailToken/:token', component: EmailVerifyComponent },
	{ path: 'emailConfirm/:token', component: EmailVerifyComponent },
	{ path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
	
	{ path: 'verifiedEmployees', component: EmployeesComponent, canActivate: [AuthGuard] },
	{ path: 'newEmployees', component: EmployeesComponent, canActivate: [AuthGuard] },
	{ path: 'unVerifiedEmployees', component: EmployeesComponent, canActivate: [AuthGuard] },

	{ path: 'editEmployee/:id', component: EditEmployeesComponent, canActivate: [AuthGuard] },
	{ path: 'employers', component: EmployersComponent, canActivate: [AuthGuard] },
	{ path: 'newEmployers', component: EmployersComponent, canActivate: [AuthGuard] },
	{ path: 'editEmployer/:id', component: EditEmployerComponent, canActivate: [AuthGuard] },
	{ path: 'categories', component: CategoriesComponent, canActivate: [AuthGuard] },
	{ path: 'certificates', component: CertificatesComponent, canActivate: [AuthGuard] },
	{ path: 'jobs/posted', component: JobsComponent, canActivate: [AuthGuard] },
	{ path: 'jobs/past', component: JobsComponent, canActivate: [AuthGuard] },
	{ path: 'jobs/upcoming', component: JobsComponent, canActivate: [AuthGuard] },
	{ path: 'jobs/ongoing', component: JobsComponent, canActivate: [AuthGuard] },
	{ path: 'jobs/getEmployee/:jobType/:jobId', component: JobEmployeesComponent, canActivate: [AuthGuard] },
	{ path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuard] },
	{ path: 'contact-us', component: ContactUsComponent, canActivate: [AuthGuard] },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
