import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonServiceService } from './../common-service/common-service.service';
@Injectable({
	providedIn: 'root'
})
export class EmployeesService {


	constructor(private http: HttpClient, private commonService: CommonServiceService) { }

	getEmployees(filters) {
		return this.http.get(`${environment.apiUrl}admin/getEmployees?limit=${filters.limit}&skip=${filters.offset}&type=${filters.type}`, { headers: this.commonService.getUserToken() });
	}

	blockEmployee(data) {
		return this.http.put(`${environment.apiUrl}admin/blockUser`, data, { headers: this.commonService.getUserToken() });
	}

	verifyEmployee(data) {
		return this.http.put(`${environment.apiUrl}admin/verifyUser`, data, { headers: this.commonService.getUserToken() });
	}

	getEmpProfile(empId) {
		return this.http.get(`${environment.apiUrl}employee/userDetails?userId=${empId}`, { headers: this.commonService.getUserToken() });
	}

	getJobSkills(indId) {
		return this.http.get(`${environment.apiUrl}job/getAllEmployeeCategory?industryId=${indId}&categoryName=SKILL&limit=5000&skip=0`, { headers: this.commonService.getUserToken() });
	}

	getJobTasks(indId) {
		return this.http.get(`${environment.apiUrl}job/getAllEmployeeCategory?industryId=${indId}&categoryName=TASK&limit=5000&skip=0`, { headers: this.commonService.getUserToken() });
	}
	getJobAbilities(indId) {
		return this.http.get(`${environment.apiUrl}job/getAllEmployeeCategory?industryId=${indId}&categoryName=ABILITY&limit=5000&skip=0`, { headers: this.commonService.getUserToken() });
	}

	getEmployeeDetail(empId) {
		return this.http.get(`${environment.apiUrl}employer/userAccountDetails?userId=${empId}`, { headers: this.commonService.getUserToken() });
	}
	updateEmpoyee(data){
		return this.http.put(`${environment.apiUrl}admin/employee/editProfile`, data, { headers: this.commonService.getUserTokenFormData() });
	}
}



