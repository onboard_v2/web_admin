import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonServiceService } from './../common-service/common-service.service';
@Injectable({
	providedIn: 'root'
})
export class CertificatesService {


	constructor(private http: HttpClient, private commonService: CommonServiceService) { }

	getCertificates(filters) {
		return this.http.get(`${environment.apiUrl}job/getAllCategory?categoryName=CERTIFICATE&limit=${filters.limit}&skip=${filters.offset}`, { headers: this.commonService.getUserToken() });
	}
	addCertificate(data){
		return this.http.post(`${environment.apiUrl}job/addCategory`,data, { headers: this.commonService.getUserToken() });
	}

	updateCertificate(data){
		return this.http.put(`${environment.apiUrl}job/updateCategory`,data, { headers: this.commonService.getUserToken() });
	}

	deleteCertificate(data){
        const httpOptions = {
            headers: this.commonService.getUserToken(),
            body: data
        };
		return this.http.delete(`${environment.apiUrl}job/deleteCategory`, httpOptions);
	}

}



