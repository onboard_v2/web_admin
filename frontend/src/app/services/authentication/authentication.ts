import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../models/user';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
@Injectable({ providedIn: 'root' })

export class AuthenticationService {

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient, private router: Router) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        console.log("this.currentUserSubject.value", this.currentUserSubject.value);
        return this.currentUserSubject.value;
    }

    login(data) {
        let httpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json').set('Content-Language', 'en').set('utcoffset', '-330');
        return this.http.post<any>(`${environment.apiUrl}user/login`, data, { headers: httpHeaders })
            .pipe(map(user => {
                if (user.statusCode === 200) {
                    localStorage.setItem('currentUser', JSON.stringify(user.data));
                    this.currentUserSubject.next(user);
                }
                return user;
            }));
    }

    signUp(data) {
        // let httpHeaders = new HttpHeaders()
        //     .set('Content-Language', 'en').set('utcoffset', '-330');
        // return this.http.post(`${environment.apiUrl}employer/registerEmail`, data, { headers: httpHeaders });

        let httpHeaders = new HttpHeaders().set('Content-Language', 'en').set('utcoffset', '-330');
        return this.http.post<any>(`${environment.apiUrl}employer/registerEmail`, data, { headers: httpHeaders })
            .pipe(map(user => {
                // console.log("user signup", user);
                if (user.statusCode === 200) {
                    localStorage.setItem('currentUser', JSON.stringify(user.data));
                    this.currentUserSubject.next(user);
                }
                return user;
            }));

    }

    verfiyEmail(data, emailToken) {
        return this.http.post<any>(`${environment.apiUrl}user/verifyEmail?emailToken=${emailToken}`, data);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.router.navigate(['/login']);
    }

    forgetPassword(data) {
        let httpHeaders = new HttpHeaders().set('Content-Language', 'en');
        return this.http.post<any>(`${environment.apiUrl}user/forgotPasswordEmail`, data, { headers: httpHeaders });
    }

}