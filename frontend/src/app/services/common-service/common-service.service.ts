import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class CommonServiceService {

	constructor(private http: HttpClient) { }
	getUserToken() {
		const userToken = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')) : null;
		// console.log("userToken", userToken.token);
		return new HttpHeaders()
			.set("Content-Type", "application/json").set('utcoffset', '-330').set('Content-Language', 'en').set('Authorization', userToken ? `Bearer ${userToken.token}` : '')
	}

	getUserTokenFormData() {
		const userToken = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')) : null;
		return new HttpHeaders()
			.set('utcoffset', '-330').set('Content-Language', 'en').set('Authorization', userToken ? `Bearer ${userToken.token}` : '')
	}

	// getUserTokenWithBody(data){
	// 	console.log("getUserTokenWithBodydata", data);
	// 	const userToken = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')) : null;
	// 	return new HttpHeaders()
	// 		.set("Content-Type", "application/json").set('utcoffset', '-330').set('Content-Language', 'en').set('Authorization', userToken ? `Bearer ${userToken.token}` : '').set('body', data)
	// }
}


