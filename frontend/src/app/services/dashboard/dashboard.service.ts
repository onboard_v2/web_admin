import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonServiceService } from './../common-service/common-service.service';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient, private commonService: CommonServiceService) { }

	getDashboardCount(filters) {
		return this.http.get(`${environment.apiUrl}admin/getDashboardCount?startDate=${filters.startDate}&endDate=${filters.endDate}`, { headers: this.commonService.getUserToken() });
	}

	getEmployees(filters) {
		return this.http.get(`${environment.apiUrl}admin/getEmployees?startDate=${filters.startDate}&endDate=${filters.endDate}&type=ALL`, { headers: this.commonService.getUserToken() });
	}

	getEmployers(filters) {
		return this.http.get(`${environment.apiUrl}admin/getEmployers?startDate=${filters.startDate}&endDate=${filters.endDate}&type=ALL`, { headers: this.commonService.getUserToken() });
	}

	getJobListing(filters) {
		return this.http.get(`${environment.apiUrl}admin/jobListing?startDate=${filters.startDate}&endDate=${filters.endDate}&limit=500`, { headers: this.commonService.getUserToken() });
	}

	

}
