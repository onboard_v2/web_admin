import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonServiceService } from './../common-service/common-service.service';
@Injectable({
	providedIn: 'root'
})
export class CategoriesService {


	constructor(private http: HttpClient, private commonService: CommonServiceService) { }

	getCategories(filters) {
		return this.http.get(`${environment.apiUrl}job/getAllCategory?categoryName=CATEGORY&limit=${filters.limit}&skip=${filters.offset}`, { headers: this.commonService.getUserToken() });
	}
}



