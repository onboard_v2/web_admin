import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonServiceService } from './../common-service/common-service.service';
@Injectable({
	providedIn: 'root'
})
export class JobsService {

	constructor(private http: HttpClient, private commonService: CommonServiceService) { }

	getJobs(filters) {
		if(filters.searchJobTitle != ''){
			return this.http.get(`${environment.apiUrl}admin/jobListing?limit=${filters.limit}&skip=${filters.offset}&jobType=${filters.type}&jobTitle=${filters.searchJobTitle}`, { headers: this.commonService.getUserToken() });
		} else{
			return this.http.get(`${environment.apiUrl}admin/jobListing?limit=${filters.limit}&skip=${filters.offset}&jobType=${filters.type}`, { headers: this.commonService.getUserToken() });
		}
		
	}

	getJobEmployees(filters) {
		return this.http.get(`${environment.apiUrl}job/viewApplicants?jobId=${filters.jobId}&isApplied=false&isHired=true&isUnhired=true&limit=${filters.limit}&skip=${filters.offset}`, { headers: this.commonService.getUserToken() });
	}

}



