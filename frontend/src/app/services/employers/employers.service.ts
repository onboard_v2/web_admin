import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CommonServiceService } from './../common-service/common-service.service';
@Injectable({
	providedIn: 'root'
})
export class EmployersService {


	constructor(private http: HttpClient, private commonService: CommonServiceService) { }

	getEmployers(filters) {
		return this.http.get(`${environment.apiUrl}admin/getEmployers?limit=${filters.limit}&skip=${filters.offset}&type=${filters.type}`, { headers: this.commonService.getUserToken() });
	}

	blockEmployer(data) {
		return this.http.put(`${environment.apiUrl}admin/blockUser`, data, { headers: this.commonService.getUserToken() });
	}

	getEmpProfile(empId) {
		return this.http.get(`${environment.apiUrl}employer/userDetails?userId=${empId}`, { headers: this.commonService.getUserToken() });
	}

	updateEmpoyer(data) {
		return this.http.put(`${environment.apiUrl}admin/employer/editProfile`, data, { headers: this.commonService.getUserTokenFormData() });
	}

	getJobLocation(loc) {
		return this.http.get(`${environment.apiUrl}job/getPlaces?keyword=${loc}`, { headers: this.commonService.getUserToken() });
	}

}



