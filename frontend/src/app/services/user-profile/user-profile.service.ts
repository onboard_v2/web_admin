import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { AuthenticationService } from '../authentication/authentication';
import { CommonServiceService } from './../common-service/common-service.service';

@Injectable({
	providedIn: 'root'
})
export class UserProfileService {

	constructor(private http: HttpClient, private authenticationService: AuthenticationService, private commonService: CommonServiceService) { }

	getProfile(userId) {
		return this.http.get(`${environment.apiUrl}employer/userDetails?userId=${userId}`, { headers: this.commonService.getUserToken() });
	}

	updatePassword(data) {
		return this.http.put(`${environment.apiUrl}user/changePassword`, data, { headers: this.commonService.getUserTokenFormData() });
	}

	updateProfie(data) {
		return this.http.put(`${environment.apiUrl}employer/editProfile`, data, { headers: this.commonService.getUserTokenFormData() });
	}
	resetPassword(data) {
		let httpHeaders = new HttpHeaders().set('content-language', 'en');
		return this.http.put(`${environment.apiUrl}user/resetPassword`, data, { headers: httpHeaders });
	}

	resetPasswordValidator(data) {
		let httpHeaders = new HttpHeaders().set('content-language', 'en');
		return this.http.post(`${environment.apiUrl}user/validateResetPassword`, data, { headers: httpHeaders });
	}

	getAllConstants() {
		return this.http.get(`${environment.apiUrl}user/getConstants`, { headers: this.commonService.getUserToken() });
	}

	updateConstant(data) {
		return this.http.put(`${environment.apiUrl}admin/updateConstant`, data, { headers: this.commonService.getUserTokenFormData() });
	}

	sendNotifications(data){
		return this.http.post(`${environment.apiUrl}admin/sendNotifications`, data, { headers: this.commonService.getUserTokenFormData() });
	}

}
