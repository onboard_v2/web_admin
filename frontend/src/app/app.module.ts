import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';

import { SidebarComponent } from './components/common/sidebar/sidebar.component';
import { UserMenuComponent } from './components/common/user-menu/user-menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { Ng5SliderModule } from 'ng5-slider';
import { ToastrModule } from 'ngx-toastr';

import { ChangePasswordComponent } from './components/change-password/change-password.component';

import { SafeHtmlPipe } from "./pipes/pipe.safehtml";
import { EmailVerifyComponent } from './components/email-verify/email-verify.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { EllipsisPipe } from './pipes/ellipsis.pipe';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { EmployersComponent } from './components/employers/employers.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CertificatesComponent } from './components/certificates/certificates.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { NewEmployeesComponent } from './components/employees/new-employees/new-employees.component';
import { EditEmployeesComponent } from './components/employees/edit-employees/edit-employees.component';
import { EditEmployerComponent } from './components/employers/edit-employer/edit-employer.component';
import { JobEmployeesComponent } from './components/jobs/job-employees/job-employees.component';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        LoginComponent,
        SignupComponent,
        SidebarComponent,
        UserMenuComponent,
        ChangePasswordComponent,
        SafeHtmlPipe,
        EllipsisPipe,
        EmailVerifyComponent,
        ForgetPasswordComponent,
        ResetPasswordComponent,
        DashboardComponent,
        EmployeesComponent,
        EmployersComponent,
        CategoriesComponent,
        CertificatesComponent,
        JobsComponent,
        NotificationsComponent,
        ContactUsComponent,
        NewEmployeesComponent,
        EditEmployeesComponent,
        EditEmployerComponent,
        JobEmployeesComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule,
        NgMultiSelectDropDownModule.forRoot(),
        AutocompleteLibModule,
        Ng5SliderModule,
        // FroalaEditorModule.forRoot(),
        // FroalaViewModule.forRoot(),
        ToastrModule.forRoot(),
        NgSelectModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
