import { FormControl } from '@angular/forms';
export class PasswordValidator {

  static password(control: FormControl): { [s: string]: boolean } {

        let PASSWORD_REGEXP = /^(?=.*?[A-Z])(?=.*?[a-z])(?!.*[\s])(?=.*?[0-9]).{8,}/;
        // let PASSWORD_REGEXP = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}$/;

        if (control.value && control.value !== '' && (control.value.length <= 7 || !PASSWORD_REGEXP.test(control.value))) {
            return { invalid : true };
        }
    }
}
