import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication/authentication';
import { UserProfileService } from '../../services/user-profile/user-profile.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

	submitBtn = 'Send';
	submitted = false;
	notificationForm: FormGroup;
	constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private userProfileService: UserProfileService, private toastrService: ToastrService) { }

	ngOnInit() {
		this.notificationForm = this.formBuilder.group({
			userType: ['employer', Validators.required],
			title: ['', Validators.required],
			description: ['', Validators.required]
		});
	}

	
	get f() { return this.notificationForm.controls; }
	onSubmit() {
		this.submitted = true;
		this.submitBtn = 'Sending...';
		if (this.notificationForm.invalid) {
			return;
		}
		let fd = new FormData();
		fd.append('heading', this.f.title.value);
		fd.append('body', this.f.description.value);
		fd.append('userType', this.f.userType.value);

		this.userProfileService.sendNotifications(fd)
			.subscribe((data: any) => {
				this.submitBtn = 'Send';
				if (data.statusCode === 200) {
					this.notificationForm.patchValue({
						title : '',
						description : ''
					})
					this.toastrService.success('Notification Send Successfully.');
				} else {
					this.toastrService.error(data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				this.submitBtn = 'Send';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}

	
}
