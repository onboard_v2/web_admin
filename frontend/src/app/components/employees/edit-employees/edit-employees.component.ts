import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeesService } from '../../../services/employees/employees.service';
import { AuthenticationService } from '../../../services/authentication/authentication';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
	selector: 'app-edit-employees',
	templateUrl: './edit-employees.component.html',
	styleUrls: ['./edit-employees.component.css']
})
export class EditEmployeesComponent implements OnInit {

	empId = '';
	name = '';
	profilePic = '';
	submitBtn = 'Save';
	submitted = false;
	empForm: FormGroup;
	uploadForm: FormGroup;

	isAddSkill = true;
	tempSkill = [];
	skills = [];
	jobSkill = [];
	// jobSkill = [];
	jobSkillID = [];
	industryId = '';
	skillSearch = '';

	isAddTask = true;
	tempTask = [];
	tasks = [];
	jobTask = [];
	jobTaskID = [];
	taskSearch = '';


	isAddAbility = true;
	tempAbility = [];
	abilities = [];
	jobAbility = [];
	jobAbilityID = [];
	abilitySearch = '';

	resumePic = [];
	verifyBtn = "Verify";
	verifyStatus = false;
	constructor(private router : Router, private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private employeesService: EmployeesService, private route: ActivatedRoute, private toastrService: ToastrService) { }

	ngOnInit() {

		this.empId = this.route.snapshot.url[1].path;
		this.empForm = this.formBuilder.group({
			name : ['', Validators.required],
			email: ['', Validators.required],
			phone: ['', ''],
			street: ['', ''],
			suburb: ['', ''],
			region: ['', ''],
			postal: ['', ''],
			country: ['', ''],
			industry: ['', Validators.required],
			skillSearch: ['', ''],
			taskSearch: ['', ''],
			abilitySearch: ['', '']
		});

		this.uploadForm = this.formBuilder.group({
			image: ['']
		});
		this.getEmpProfile();
	}

	getEmpProfile() {

		this.employeesService.getEmpProfile(this.empId).subscribe((emp: any) => {
			// console.log("emp", emp);
			if (emp && emp.statusCode == 200) {
				this.name = emp.data.name,
				this.empForm.patchValue({
					name: emp.data.name,
					email: emp.data.email,
					phone: emp.data.phone,
					street: (emp.data.employee.accountDetails != null) ? emp.data.employee.accountDetails.street : '',
					suburb: (emp.data.employee.accountDetails != null) ? emp.data.employee.accountDetails.suburb : '',
					postal: (emp.data.employee.accountDetails != null) ? emp.data.employee.accountDetails.pOBox : '',
					region: (emp.data.employee.accountDetails != null) ? emp.data.employee.accountDetails.region : '',
					country: (emp.data.employee.accountDetails != null) ? emp.data.employee.accountDetails.country : '',
					industry: (emp.data.employee.industry != null) ? emp.data.employee.industry.description : ''
				});

				this.profilePic = (emp.data.profilePic != null) ? emp.data.profilePic : '';
				this.resumePic = (emp.data.employee != null) ? emp.data.employee.resumes : [];
				let skillsData = (emp.data.employee != null) ? emp.data.employee.skills : [];
				let taskData = (emp.data.employee != null) ? emp.data.employee.tasks : [];
				let abilitiesData = (emp.data.employee != null) ? emp.data.employee.abilities : [];

				this.industryId = (emp.data.employee.industry != null) ? emp.data.employee.industry._id : '';
				this.getJobSkills();
				this.getJobTasks();
				this.getJobAbilities();
				this.jobSkill = skillsData.map(s => {
					return s.description;
				});
				this.jobTask = taskData.map(t => {
					return t.description
				});
				this.jobAbility = abilitiesData.map(a => {
					return a.description;
				});

				this.jobSkillID = skillsData.map(s => {
					return s._id;
				});
				this.jobTaskID = taskData.map(t => {
					return t._id
				});
				this.jobAbilityID = abilitiesData.map(a => {
					return a._id;
				});



			} else {
				this.toastrService.error('Error in Api');
			}
		}, error => {
			this.toastrService.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}


	/*********** Start Add Skill Section **************/
	addSkill() {
		this.isAddSkill = false;
	}
	removeSkillBox() {
		this.isAddSkill = true;
	}
	getJobSkills() {
		this.employeesService.getJobSkills(this.industryId).subscribe((skills: any) => {
			console.log('skills', skills);
			if (skills && skills.statusCode == 200) {
				this.skills = skills.data.categoryData;
				this.tempSkill = skills.data.categoryData;
			}
		});
	}
	onChangeSkill(skillEvent) {
		let skillFilter = this.tempSkill.filter(skl => {
			if (skl && skl.description) {
				return skl.description.toLowerCase().indexOf(skillEvent.target.value) >= 0;
			} else {
				return skillFilter;
			}
		});
		this.skills = skillFilter;
	}

	skillsFunc(skill, skillID) {

		this.isAddSkill = true;
		// this.skills = this.tempSkill;
		if (this.jobSkill.indexOf(skill) === -1) {
			this.jobSkill.push(skill);
		}
		if (this.jobSkillID.indexOf(skillID) === -1) {
			this.jobSkillID.push(skillID);
		}
		let sb = this.removeFromArray(this.tempSkill, this.jobSkill);
		this.skills = sb;
		this.empForm.patchValue({
			'skillSearch': ''
		});
	}
	removeSkill(skill) {
		console.log("skill", skill);
		this.jobSkill = this.jobSkill.filter(e => e !== skill);
		console.log("this.jobSkill", this.jobSkill);
	}
	/*********End Skill Section *************/

	/*********Start Task Section ************/

	addTask() {
		this.isAddTask = false;
	}
	removeTaskBox() {
		this.isAddTask = true;
	}
	getJobTasks() {
		this.employeesService.getJobTasks(this.industryId).subscribe((tasks: any) => {

			if (tasks && tasks.statusCode == 200) {
				this.tasks = tasks.data.categoryData;
				this.tempTask = tasks.data.categoryData;
			}
		});
	}
	onChangeTask(taskEvent) {
		let taskFilter = this.tempTask.filter(skl => {
			if (skl && skl.description) {
				return skl.description.toLowerCase().indexOf(taskEvent.target.value) >= 0;
			} else {
				return taskFilter;
			}
		});
		this.tasks = taskFilter;
	}

	tasksFunc(task, taskID) {

		this.isAddTask = true;
		if (this.jobTask.indexOf(task) === -1) {
			this.jobTask.push(task);
		}
		if (this.jobTaskID.indexOf(taskID) === -1) {
			this.jobTaskID.push(taskID);
		}
		let sb = this.removeFromArray(this.tempTask, this.jobTask);
		this.tasks = sb;
		this.empForm.patchValue({
			'taskSearch': ''
		});
	}

	removeTask(task) {
		this.jobTask = this.jobTask.filter(e => e !== task);
	}

	/*********End Task Section **************/

	/*********Start Ability Section ************/

	addAbility() {
		this.isAddAbility = false;
	}
	removeAbilityBox() {
		this.isAddAbility = true;
	}
	getJobAbilities() {
		this.employeesService.getJobAbilities(this.industryId).subscribe((abilities: any) => {

			if (abilities && abilities.statusCode == 200) {
				this.abilities = abilities.data.categoryData;
				this.tempAbility = abilities.data.categoryData;
			}
		});
	}
	onChangeAbility(abilityEvent) {
		let abilityFilter = this.tempAbility.filter(skl => {
			if (skl && skl.description) {
				return skl.description.toLowerCase().indexOf(abilityEvent.target.value) >= 0;
			} else {
				return abilityEvent;
			}
		});
		this.abilities = abilityEvent;
	}

	abilitiesFunc(ability, abilityID) {

		this.isAddAbility = true;
		if (this.jobAbility.indexOf(ability) === -1) {
			this.jobAbility.push(ability);
		}
		if (this.jobAbilityID.indexOf(abilityID) === -1) {
			this.jobAbilityID.push(abilityID);
		}
		let sb = this.removeFromArray(this.tempAbility, this.jobAbility);
		this.abilities = sb;
		this.empForm.patchValue({
			'abilitySearch': ''
		});
	}

	removeAbility(ability) {
		this.jobAbility = this.jobAbility.filter(e => e !== ability);
	}

	/*********End Ability Section **************/


	removeFromArray(original, remove) {
		// console.log('original', original, 'remove', remove);
		return original.filter(value => !remove.includes(value.description));
	}

	get f() { return this.empForm.controls; }
	onSubmit(){
		console.log("this.empForm.value", this.empForm.value);
		this.submitted = true;
		this.submitBtn = 'Updating...';

		if (this.empForm.invalid) {
			return;
		}

		let objData = {
		  "userId": this.empId,
		  "countryCode" : "string",
		  "phone" : this.f.phone.value,
		  "email" : this.f.email.value,
		  "accountDetails": {
		  	"phone": this.f.phone.value,
		    "street": this.f.street.value,
		    "suburb": this.f.suburb.value,
		    "country": this.f.country.value,
		    "region": this.f.region.value,
		    "pOBox": this.f.postal.value
		  },
		  "skills": JSON.stringify(this.jobSkillID),
		  "abilities": JSON.stringify(this.jobAbilityID),
		  "tasks": JSON.stringify(this.jobTaskID)
		}
		this.employeesService.updateEmpoyee(objData)
			.subscribe((data: any) => {
				this.submitBtn = 'Save';
				if (data.statusCode === 200) {
					this.toastrService.success('Employee Updated.');
					this.router.navigate(['/employees']);
				} else {
					this.toastrService.error(data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				this.submitBtn = 'Save';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}


  
  // verify
	verifyCustomer() {
		let userId = {
			userId: this.empId
		}
		this.employeesService.verifyEmployee(userId).subscribe((result: any) => {
			if (result && result.statusCode == 200) {
				this.toastrService.success(`Employee Verified Successfully`);
				this.verifyBtn = 'Verified';
				this.verifyStatus = true;
			} else {
				this.toastrService.error('Error in Api');
			}
		}, (error) => {
			this.toastrService.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		}
		);

	}

}
