import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication/authentication';
import { EmployeesService } from '../../services/employees/employees.service';
import { PageService } from '../../services/pagination/page.service';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
	selector: 'app-employees',
	templateUrl: './employees.component.html',
	styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

	jobEmployees = [];
	jobEmployeesCount = 0;
	userType = 'verified'
	loadingMsg = 'Loading Employees.';
	blockText = 'block';
	userId = '';
	pageIndex;
	totalPages = [];
	pager: any = {};
	config: any;
	page = 15;
	offset = 0;
	pagination = {
		count: this.page,
		page: 1,
		pages: 0,
		size: 0
	};
	filters = {
		page_no: 1,
		limit: this.page,
		offset: this.offset,
		type: 'VERIFIED'
	}
	empData = [];
	profilePic = '';
	resumePic = [];
	skillsData = [];
	taskData = [];
	abilitiesData = [];
	address = '';
	aboutMe = '';
	profession: any;
	availability = [];
	previousEmployments = [];
	documents = [];
	taskLimit = 2;
	skillLimit = 3;
	abilityLimit = 2;
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private employeesService: EmployeesService,
		@Inject(DOCUMENT) private document: Document,
		private renderer: Renderer2,
		private pageService: PageService,
		private toastr: ToastrService,
		private route: ActivatedRoute) {
		this.config = {
			currentPage: 1,
			itemsPerPage: this.page
		};
		this.pagination = {
			count: this.page,
			page: 1,
			pages: 0,
			size: 0
		};
	}

	ngOnInit() {
		if (this.route.snapshot.url[0].path == 'unVerifiedEmployees') {
			this.filters.type = 'UNVERIFIED';
			this.userType = 'unverified';
		} else if (this.route.snapshot.url[0].path == 'newEmployees') {
			this.filters.type = 'UNCOMPLETE';
			this.userType = 'uncomplete';
		} else{
			this.filters.type = 'VERIFIED';
			this.userType = 'verified';
		}
		this.getEmployees(this.filters);
	}


	getEmployees(filterData) {

		this.employeesService.getEmployees(filterData).subscribe((employees: any) => {
			// console.log("employees", employees);

			this.loadingMsg = 'Employees Not Found';
			if (employees && employees.statusCode == 200) {
				this.jobEmployees = employees.data.users;
				this.jobEmployeesCount = employees.data.count;
				let tPages = Math.ceil(this.jobEmployeesCount / this.config.itemsPerPage);
				this.totalPages = Array(tPages).fill(tPages).map((x, i) => i + 1);
				this.pagination.size = this.jobEmployeesCount;
				this.pager = this.pageService.getPager(this.jobEmployeesCount, this.filters.page_no, this.pagination.count);
				this.pageIndex = (this.pager.currentPage - 1) * this.filters.limit;
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.loadingMsg = 'Employees Not Found';
			// this.renderer.removeClass(this.document.body, 'show-spinner');
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
			// console.log("error", error);
		});
	}

	setPage(pagenumber: number) {
		this.pagination.page = pagenumber;
		this.filters.page_no = pagenumber;
		this.filters.offset = this.page * (pagenumber - 1);
		this.pager = this.pageService.getPager(this.pagination.size, this.pagination.page, this.pagination.count);
		this.getEmployees(this.filters,);
	}

	BlockEmp(text, empId) {
		this.blockText = (text == 'block') ? 'block' : 'unblock';
		this.userId = empId;
	}

	blockCustomerConfirm(type) {
		let block = (this.blockText == 'block') ? true : false;
		let blockMsg = (this.blockText == 'block') ? 'Blocked' : 'Unblocked';
		let data = {
			isBlocked: block,
			userId: this.userId
		}
		this.employeesService.blockEmployee(data).subscribe((employee: any) => {
			// console.log("employee", employee);
			if (employee && employee.statusCode == 200) {
				this.toastr.success(`Employee ${blockMsg} Successfully`);
				this.getEmployees(this.filters);

			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});

	}


	verifyCustomerId(id) {
		this.userId = id;
	}

	// verify
	verifyCustomer() {
		let userId = {
			userId: this.userId
		}
		this.employeesService.verifyEmployee(userId).subscribe((result: any) => {
			if (result && result.statusCode == 200) {
				this.toastr.success(`Employee Verified Successfully`);
				this.getEmployees(this.filters);

			} else {
				this.toastr.error('Error in Api');
			}
		}, (error) => {
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		}
		);

	}


	openPopup(empId) {

		this.employeesService.getEmployeeDetail(empId).subscribe((empDetail: any) => {
			if (empDetail && empDetail.statusCode == 200) {
				this.empData = empDetail.data;

				this.aboutMe = (empDetail.data.employee != null) ? empDetail.data.employee.aboutMe : '';
				this.profession = (empDetail.data.employee != null) ? empDetail.data.employee.profession : '';
				this.availability = (empDetail.data.employee != null) ? empDetail.data.employee.availability : [];
				this.previousEmployments = (empDetail.data.employee != null) ? empDetail.data.employee.previousEmployments : [];
				this.documents = (empDetail.data.employee != null) ? empDetail.data.employee.documents : [];
				this.address = (empDetail.data.address != null) ? empDetail.data.employee.street : '';
				this.profilePic = (empDetail.data.profilePic != null) ? empDetail.data.profilePic : '';
				this.resumePic = (empDetail.data.employee != null) ? empDetail.data.employee.resumes : [];
				this.skillsData = (empDetail.data.employee != null) ? empDetail.data.employee.skills : [];
				this.taskData = (empDetail.data.employee != null) ? empDetail.data.employee.tasks : [];
				this.abilitiesData = (empDetail.data.employee != null) ? empDetail.data.employee.abilities : [];

				$('#myModal').modal({ show: true, backdrop: 'static', keyboard: false })
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

	plusTask(limit) {
		this.taskLimit = limit;
	}
	minusTask() {
		this.taskLimit = 2;
	}

	plusSkill(limit) {
		this.skillLimit = limit;
	}
	minusSkill() {
		this.skillLimit = 3;
	}

	plusAbility(limit) {
		this.abilityLimit = limit;
	}
	minusAbility() {
		this.abilityLimit = 2;
	}



}

