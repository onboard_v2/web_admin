import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication';
import { EmployeesService } from '../../../services/employees/employees.service';
import { PageService } from '../../../services/pagination/page.service';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: 'app-new-employees',
  templateUrl: './new-employees.component.html',
  styleUrls: ['./new-employees.component.css']
})
export class NewEmployeesComponent implements OnInit {

	jobEmployees = [];
	jobEmployeesCount = 0;
	loadingMsg = 'Loading Employees.';
	
	totalPages = [];
	pager: any = {};
	config: any;
	page = 15;
	offset = 0;
	pagination = {
		count: this.page,
		page: 1,
		pages: 0,
		size: 0
	};
	filters = {
		page_no: 1,
		limit: this.page,
		offset: this.offset,
		type: 'UNVERIFIED'
	}
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private employeesService: EmployeesService,
		@Inject(DOCUMENT) private document: Document,
		private renderer: Renderer2,
		private pageService: PageService,
		private toastr: ToastrService,
		private route: ActivatedRoute) {
		this.config = {
			currentPage: 1,
			itemsPerPage: this.page
		};
		this.pagination = {
			count: this.page,
			page: 1,
			pages: 0,
			size: 0
		};
	}

	ngOnInit() {
		this.getEmployees(this.filters);
	}


	getEmployees(filterData) {

		this.employeesService.getEmployees(filterData).subscribe((employees: any) => {
			console.log("employees", employees);
			
			this.loadingMsg = 'Employees Not Found';
			if (employees && employees.statusCode == 200) {
				this.jobEmployees = employees.data.users;
				this.jobEmployeesCount = employees.data.count;
				let tPages = Math.ceil(this.jobEmployeesCount / this.config.itemsPerPage);
				this.totalPages = Array(tPages).fill(tPages).map((x, i) => i + 1);
				this.pagination.size = this.jobEmployeesCount;
				this.pager = this.pageService.getPager(this.jobEmployeesCount, this.filters.page_no, this.pagination.count);

			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.loadingMsg = 'Employees Not Found';
			// this.renderer.removeClass(this.document.body, 'show-spinner');
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
		        this.authenticationService.logout();
		    }
			console.log("error", error);
		});
	}

	setPage(pagenumber: number) {
		this.pagination.page = pagenumber;
		this.filters.page_no = pagenumber;
		this.filters.offset = this.page * (pagenumber - 1);
		this.pager = this.pageService.getPager(this.pagination.size, this.pagination.page, this.pagination.count);
		this.getEmployees(this.filters,);
	}
}

