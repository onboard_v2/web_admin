import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../services/authentication/authentication';
@Component({
	selector: 'app-email-verify',
	templateUrl: './email-verify.component.html',
	styleUrls: ['./email-verify.component.css']
})
export class EmailVerifyComponent implements OnInit {

	message = '';
	constructor(private router: Router, private route: ActivatedRoute, private authenticationService: AuthenticationService, @Inject(DOCUMENT) private document: Document,
		private renderer: Renderer2) { }

	ngOnInit() {
		this.renderer.addClass(this.document.body, 'background');
		this.renderer.addClass(this.document.body, 'no-footer');
		let emailToken = this.route.snapshot.params.token;
		console.log('emailToken', emailToken);
		if (emailToken) {
			this.verifyEmail(emailToken);
		}
	}

	verifyEmail(emailToken) {

		let fData: FormData = new FormData;
		fData.append("emailToken", emailToken);
		this.authenticationService.verfiyEmail(fData, emailToken)
			.pipe(first())
			.subscribe((data: any) => {
				console.log("data", data);
				if (data.statusCode === 200) {
					this.message = 'You have successfully verified with this account.';
					setTimeout(() => {
						this.router.navigate(['/login']);
					}, 5000);
				} else {
					this.message = data.message;
					// setTimeout(() => {
					// 	this.router.navigate(['/login']);
					// }, 5000);
					// console.log("this.message", this.message);
				}
			}, error => {
				this.message = error.error.message;
				// setTimeout(() => {
				// 		this.router.navigate(['/login']);
				// 	}, 5000);
				console.log("error", error.error.message);
			});

	}

}
