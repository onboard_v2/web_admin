import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication/authentication';
import { UserProfileService } from '../../services/user-profile/user-profile.service';
import { ToastrService } from 'ngx-toastr';
@Component({
	selector: 'app-contact-us',
	templateUrl: './contact-us.component.html',
	styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

	submitBtnPhone = 'Save';
	submitBtnEmail = 'Save';
	submitBtnAddress = 'Save';
	submitted = false;
	phoneForm: FormGroup;
	emailForm: FormGroup;
	addressForm: FormGroup;
	phone_id = '';
	email_id = '';
	address_id = '';
	constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private userProfileService: UserProfileService, private toastrService: ToastrService) { }

	ngOnInit() {
		this.phoneForm = this.formBuilder.group({
			phone: ['', Validators.required]
		});
		this.emailForm = this.formBuilder.group({
			email: ['', Validators.required]
		});
		this.addressForm = this.formBuilder.group({
			address: ['', Validators.required]
		});
		this.getAllConstants();
	}

	getAllConstants() {

		this.userProfileService.getAllConstants().subscribe((results: any) => {

			if (results && results.statusCode == 200) {
				this.phone_id = results.data.constants.SUPPORT_PHONE_NUMBER._id;
				this.email_id = results.data.constants.SUPPORT_EMAIL._id;
				this.address_id = results.data.constants.PHYSICAL_ADDRESS._id;
				this.phoneForm.patchValue({
					phone: results.data.constants.SUPPORT_PHONE_NUMBER.value
				});
				this.emailForm.patchValue({
					email: results.data.constants.SUPPORT_EMAIL.value
				});
				this.addressForm.patchValue({
					address: results.data.constants.PHYSICAL_ADDRESS.value
				});
			} else {
				this.toastrService.error('Error in Api');
			}
		}, error => {
			this.toastrService.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

	get ph() { return this.phoneForm.controls; }
	onSubmitPhone() {
		this.submitted = true;
		this.submitBtnPhone = 'Updating...';
		if (this.phoneForm.invalid) {
			return;
		}
		let fd = new FormData();
		fd.append('constantId', this.phone_id);
		fd.append('value', this.ph.phone.value);
		this.userProfileService.updateConstant(fd)
			.subscribe((data: any) => {
				this.submitBtnPhone = 'Save';
				if (data.statusCode === 200) {
					this.toastrService.success('Phone Number Updated.');
					this.getAllConstants();
				} else {
					this.toastrService.error(data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				this.submitBtnPhone = 'Save';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}

	get e() { return this.emailForm.controls; }
	onSubmitEmail() {
		this.submitted = true;
		this.submitBtnEmail = 'Updating...';
		if (this.emailForm.invalid) {
			return;
		}
		let fd = new FormData();
		fd.append('constantId', this.email_id);
		fd.append('value', this.e.email.value);
		this.userProfileService.updateConstant(fd)
			.subscribe((data: any) => {
				this.submitBtnEmail = 'Save';
				if (data.statusCode === 200) {
					this.toastrService.success('Email Updated.');
					this.getAllConstants();
				} else {
					this.toastrService.error(data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				this.submitBtnEmail = 'Save';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}

	get add() { return this.addressForm.controls; }
	onSubmitAddress() {
		this.submitted = true;
		this.submitBtnAddress = 'Updating...';
		if (this.addressForm.invalid) {
			return;
		}
		let fd = new FormData();
		fd.append('constantId', this.address_id);
		fd.append('value', this.add.address.value);
		this.userProfileService.updateConstant(fd)
			.subscribe((data: any) => {
				this.submitBtnAddress = 'Save';
				if (data.statusCode === 200) {
					this.toastrService.success('Physical Address Updated.');
					this.getAllConstants();
				} else {
					this.toastrService.error(data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				this.submitBtnAddress = 'Save';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}

}
