import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { AuthenticationService } from '../../services/authentication/authentication';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { ExcelService } from './../../services/excel.service';
import { ExportToCsv } from 'export-to-csv';
@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	startDate: any;
	endDate: any;
	employeeCount : any;
	employerCount : any;
	jobCount :  any;
	filterData : any;
	options = {
		filename: 'Reports',
		fieldSeparator: ',',
		quoteStrings: '"',
		decimalSeparator: '.',
		showLabels: true,
		showTitle: true,
		title: 'Reports',
		useTextFile: false,
		useBom: true,
		useKeysAsHeaders: true,
		headers: ['Name', 'Email', 'Phone', 'Verified']
	};
	jobOptions ={
		filename: 'Reports',
		fieldSeparator: ',',
		quoteStrings: '"',
		decimalSeparator: '.',
		showLabels: true,
		showTitle: true,
		title: 'Reports',
		useTextFile: false,
		useBom: true,
		useKeysAsHeaders: true,
		headers: ['JobName', 'EmployerName', 'Address', 'CurrentStatus']
	}
	empData = [];
	jobsData = [];
	constructor(private renderer: Renderer2, @Inject(DOCUMENT) private document: Document, private dashboardService: DashboardService, private toastr: ToastrService, private authenticationService:AuthenticationService,private excelService: ExcelService) { }

	ngOnInit() {
		this.renderer.removeClass(this.document.body, 'background');
		let date = new Date(), 
		y = date.getFullYear(), 
		m = date.getMonth(),
		d = date.getDate(),
		d7 = date.getDate()-7;
		
		var seven_date = new Date(y, m, d7);
		let currentDay = new Date(y, m, d);
		// console.log("firstDay", firstDay, "currentDay", currentDay);
		this.startDate = seven_date.toISOString().split('T')[0];
		this.endDate = currentDay.toISOString().split('T')[0];
		this.filterData = {
			startDate : this.startDate,
			endDate : this.endDate
		}
		this.getDashboardCount(this.filterData);
	}

	getDashboardCount(filterData) {
		this.dashboardService.getDashboardCount(filterData).subscribe((results: any) => {
			console.log("results",results);
			if (results && results.statusCode == 200) {
				this.employeeCount =  results.data.employeeCount;
				this.employerCount  = results.data.employerCount;
				this.jobCount = results.data.jobCount;

				console.log("this.employeeCount", this.employeeCount);
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

	SearchData(){
		this.filterData = {
			startDate : this.startDate,
			endDate : this.endDate
		}
		this.getDashboardCount(this.filterData);
	}

	downloadEmployeeExcel() {
		let exArray = [];
		const csvExporter = new ExportToCsv(this.options);
		this.dashboardService.getEmployees(this.filterData).subscribe((results: any) => {
			if (results && results.statusCode == 200) {
				this.empData = results.data.users;
				this.empData.map(data => {
					exArray.push({
						'Name': data.name,
						'Email': data.email,
						'Phone': data.phone,
						'Verified Status': (data.isAdminVerified) ? 'Active' : 'InActive'
					});
				})
				const fileName = `employees`;
				this.excelService.exportAsExcelFile(exArray, fileName);
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

	downloadEmployerExcel() {
		let exArray = [];
		const csvExporter = new ExportToCsv(this.options);
		this.dashboardService.getEmployers(this.filterData).subscribe((results: any) => {
			console.log("results", results);
			if (results && results.statusCode == 200) {
				this.empData = results.data.users;
				this.empData.map(data => {
					exArray.push({
						'JobName': data.name,
						'Email': data.email,
						'Phone': data.phone,
						'Verified Status': (data.isAdminVerified) ? 'Active' : 'InActive'
					});
				})
				const fileName = `employers`;
				this.excelService.exportAsExcelFile(exArray, fileName);
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

	downloadJobsExcel() {
		let exArray = [];
		const csvExporter = new ExportToCsv(this.jobOptions);
		this.dashboardService.getJobListing(this.filterData).subscribe((results: any) => {
			console.log("results", results);
			if (results && results.statusCode == 200) {
				this.jobsData = results.data.jobs;
				this.jobsData.map(data => {
					exArray.push({
						'JobName': data.jobName,
						'EmployerName': data.employer.name,
						'Address': data.address.text,
						'CurrentStatus': data.currentStatus
					});
				})
				const fileName = `jobs`;
				this.excelService.exportAsExcelFile(exArray, fileName);
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

}
