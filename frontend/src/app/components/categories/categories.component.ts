import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../../services/authentication/authentication';
import { CategoriesService } from '../../services/categories/categories.service';
import { PageService } from '../../services/pagination/page.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

	categories = [];
	categoriesCount = 0;
	loadingMsg = 'Loading Categories.';
	pageIndex;
	totalPages = [];
	pager: any = {};
	config: any;
	page = 30;
	offset = 0;
	pagination = {
		count: this.page,
		page: 1,
		pages: 0,
		size: 0
	};
	filters = {
		page_no: 1,
		limit: this.page,
		offset: this.offset
	}
	constructor(
		private authenticationService: AuthenticationService,
		private categoriesService: CategoriesService,
		private pageService: PageService,
		private toastr: ToastrService) {
		this.config = {
			currentPage: 1,
			itemsPerPage: this.page
		};
		this.pagination = {
			count: this.page,
			page: 1,
			pages: 0,
			size: 0
		};
	}

	ngOnInit() {
		this.getCategories(this.filters);
	}


	getCategories(filterData) {

		this.categoriesService.getCategories(filterData).subscribe((categories: any) => {
			this.loadingMsg = 'Categories Not Found';
			console.log("categories", categories);
			if (categories && categories.statusCode == 200) {
				this.categories = categories.data.categoryData;
				this.categoriesCount = categories.data.count;
				let tPages = Math.ceil(this.categoriesCount / this.config.itemsPerPage);
				this.totalPages = Array(tPages).fill(tPages).map((x, i) => i + 1);
				this.pagination.size = this.categoriesCount;
				this.pager = this.pageService.getPager(this.categoriesCount, this.filters.page_no, this.pagination.count);
				this.pageIndex = (this.pager.currentPage - 1) * this.filters.limit;
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.loadingMsg = 'Categories Not Found';
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

	setPage(pagenumber: number) {
		this.pagination.page = pagenumber;
		this.filters.page_no = pagenumber;
		this.filters.offset = this.page * (pagenumber - 1);
		this.pager = this.pageService.getPager(this.pagination.size, this.pagination.page, this.pagination.count);
		this.getCategories(this.filters);
	}
}


