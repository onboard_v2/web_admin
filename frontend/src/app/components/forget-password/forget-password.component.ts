import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication/authentication';
import { ToastrService } from 'ngx-toastr';
import { EmailValidator, PasswordValidator } from '../../validators';
@Component({
	selector: 'app-forget-password',
	templateUrl: './forget-password.component.html',
	styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

	ForgetForm: FormGroup;
	loading = false;
	submitted = false;
	returnUrl: string;
	error = '';
	loginBtn = 'Forgot password';
	userFirstname = '';
	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		@Inject(DOCUMENT) private document: Document,
		private renderer: Renderer2,
		private toastrService: ToastrService
	) {
		// redirect to home if already logged in
		if (this.authenticationService.currentUserValue) {
			this.router.navigate(['/new-job-post']);
		}
	}

	ngOnInit() {
		// this.renderer.removeClass(this.document.body, 'embedded-body');
		this.renderer.addClass(this.document.body, 'background');
		this.renderer.addClass(this.document.body, 'no-footer');
		this.ForgetForm = this.formBuilder.group({
			email: ['', Validators.compose([Validators.required, EmailValidator.email])],
		});

		// get return url from route parameters or default to '/'
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/new-job-post';
	}

	signupBtn() {
		this.router.navigateByUrl('/register');
	}

	signinBtn() {
		this.router.navigateByUrl('/login');
	}
	// convenience getter for easy access to form fields
	get f() { return this.ForgetForm.controls; }

	onSubmit() {
		this.submitted = true;

		// stop here if form is invalid
		if (this.ForgetForm.invalid) {
			return;
		}
		this.loginBtn = 'Loading...';
		this.loading = true;
		let data = {
			email: this.f.email.value,
		};
		this.authenticationService.forgetPassword(data)
			.pipe(first())
			.subscribe(data => {
				console.log("data", data);
				if (data.statusCode === 200) {
					this.toastrService.success('Password Reset Link Sent.', 'Success!');
					this.router.navigate([this.returnUrl]);
				} else {
					this.loading = false;
					this.loginBtn = 'Forgot password';
					this.error = data.message;;
					console.log("Error", data.message);
				}
			}, error => {
				console.log("error", error);
				this.error = error.message;
				this.toastrService.error(error.error.message);
				this.loading = false;
				this.loginBtn = 'Forgot password';
			});
	}



}

