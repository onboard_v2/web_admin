import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployersService } from '../../../services/employers/employers.service';
import { AuthenticationService } from '../../../services/authentication/authentication';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
	selector: 'app-edit-employer',
	templateUrl: './edit-employer.component.html',
	styleUrls: ['./edit-employer.component.css']
})
export class EditEmployerComponent implements OnInit {

	empId = '';
	name = '';
	profilePic: any;
	submitted = false;
	submitBtn = 'Save';
	empForm: FormGroup;
	uploadForm: FormGroup;
	jobLocation = [];
	locObj: any;
	constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private employersService: EmployersService, private route: ActivatedRoute, private toastrService: ToastrService, private router: Router) { }

	ngOnInit() {

		this.empId = this.route.snapshot.url[1].path;
		this.empForm = this.formBuilder.group({
			// company_name: ['', Validators.required],
			trading_name: ['', Validators.required],
			// parent_entity: ['', ''],
			abn_number: ['', ''],
			address: ['', ''],
			email: ['', Validators.required],
			// country: ['', ''],
			phone: ['', ''],
			// region: ['', ''],
			// payment_type: ['CARD', '']
		});

		this.uploadForm = this.formBuilder.group({
			image: ['']
		});
		this.getEmpProfile();
	}

	locKeyword = 'formatted_address';
	getJobLocation(search: string) {
		if (search && (search.length > 1)) {
			this.employersService.getJobLocation(search).subscribe((locations: any) => {
				if (locations && locations.statusCode == 200) {
					this.jobLocation = locations.data.predictions;
				}
			});
		} else {
			this.jobLocation = [];
		}
	}

	selectEvent(item) {
		console.log("item", item);
		this.jobLocation = [];
	}

	getEmpProfile() {

		this.employersService.getEmpProfile(this.empId).subscribe((emp: any) => {
			// console.log("emp", emp);
			if (emp && emp.statusCode == 200) {
				this.name = emp.data.name;
				this.empForm.patchValue({
					company_name: emp.data.name,
					trading_name: emp.data.employer.tradingName,
					parent_entity: emp.data.employer.parentEntity,
					abn_number: emp.data.employer.abnNumber,
					address: emp.data.address.street,
					email: emp.data.email,
					country: emp.data.address.country,
					phone: emp.data.phone,
					region: emp.data.address.region,
					payment_type: emp.data.employer.paymentType
				});
				this.profilePic = emp.data.profilePic;
				this.locObj = {
					"street": emp.data.address.street
				};
			} else {
				this.toastrService.error('Error in Api');
			}
		}, error => {
			this.toastrService.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

	get f() { return this.empForm.controls; }

	onSubmit() {
		this.submitted = true;
		this.submitBtn = 'Updating...';

		if (this.empForm.invalid) {
			return;
		}

		let fd: FormData = new FormData;
		fd.append('userId', this.empId);
		// fd.append('paymentType', this.f.payment_type.value);
		fd.append('email', this.f.email.value);
		if (this.f.phone.value) {
			fd.append('phone', this.f.phone.value);
			// fd.append('countryCode', '+91');
		}
		if (this.uploadForm.get('image').value) {
			fd.append('profilePic', this.uploadForm.get('image').value);
		}
		if (this.f.trading_name.value) {
			fd.append('tradingName', this.f.trading_name.value);
		}
		// if (this.f.parent_entity.value) {
		// 	fd.append('parentEntity', this.f.parent_entity.value);
		// }
		if (this.f.abn_number.value) {
			fd.append('abnNumber', this.f.abn_number.value);
		}

		// if (this.f.address.value || this.f.country.value || this.f.region.value) {
		// 	let address = {
		// 		'street': this.f.address.value,
		// 		'region': this.f.region.value,
		// 		'country': this.f.country.value,
		// 	};
		// 	if (!this.f.address.value) {
		// 		delete address['street'];
		// 	}
		// 	if (!this.f.region.value) {
		// 		delete address['region'];
		// 	}
		// 	if (!this.f.country.value) {
		// 		delete address['country'];
		// 	}
		// 	fd.append('address', JSON.stringify(address));
		// }
		if (this.f.address.value && this.f.address.value.formatted_address) {
			this.locObj = {
				"street": this.f.address.value ? this.f.address.value.formatted_address : '',
			}
		}
		console.log("this.locObj", this.locObj);
		fd.append("address", JSON.stringify(this.locObj));

		// console.log("fd", fd);
		this.employersService.updateEmpoyer(fd)
			.subscribe((data: any) => {
				this.submitBtn = 'Save';
				if (data.statusCode === 200) {
					this.toastrService.success('Employer Profile Updated.');
					this.router.navigate(['/employers']);
				} else {
					this.toastrService.error(data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				this.submitBtn = 'Save';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}

	imgUpload() {
		document.getElementById('my_file').click();
	}

	onSelectFile(event) {


		if (event.target.files && event.target.files[0]) {
			this.uploadForm.get('image').setValue(event.target.files[0]);
			let fileReader = new FileReader();
			fileReader.readAsDataURL(event.target.files[0]); // read file as data url
			fileReader.onload = (evt: Event) => { // called once readAsDataURL is completed
				this.profilePic = fileReader.result;
			};
		}
	}

}
