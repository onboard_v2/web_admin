import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication/authentication';
import { EmployersService } from '../../services/employers/employers.service';
import { PageService } from '../../services/pagination/page.service';
import { ToastrService } from 'ngx-toastr';
@Component({
	selector: 'app-employers',
	templateUrl: './employers.component.html',
	styleUrls: ['./employers.component.css']
})
export class EmployersComponent implements OnInit {

	jobEmployers = [];
	jobEmployersCount = 0;
	userType = 'verified'
	loadingMsg = 'Loading Employers.';
	blockText = 'block';
	userId = '';
	pageIndex;
	totalPages = [];
	pager: any = {};
	config: any;
	page = 15;
	offset = 0;
	pagination = {
		count: this.page,
		page: 1,
		pages: 0,
		size: 0
	};
	filters = {
		page_no: 1,
		limit: this.page,
		offset: this.offset,
		type: 'VERIFIED'
	}
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private employersService: EmployersService,
		@Inject(DOCUMENT) private document: Document,
		private renderer: Renderer2,
		private pageService: PageService,
		private toastr: ToastrService,
		private route: ActivatedRoute) {
		this.config = {
			currentPage: 1,
			itemsPerPage: this.page
		};
		this.pagination = {
			count: this.page,
			page: 1,
			pages: 0,
			size: 0
		};
	}

	ngOnInit() {
		if (this.route.snapshot.url[0].path == 'newEmployers') {
			this.filters.type = 'UNVERIFIED';
			this.userType = 'unverified';
		}
		this.getEmployers(this.filters);
	}


	getEmployers(filterData) {

		this.employersService.getEmployers(filterData).subscribe((employers: any) => {

			this.loadingMsg = 'Employer Not Found';
			if (employers && employers.statusCode == 200) {
				this.jobEmployers = employers.data.users;
				this.jobEmployersCount = employers.data.count;
				let tPages = Math.ceil(this.jobEmployersCount / this.config.itemsPerPage);
				this.totalPages = Array(tPages).fill(tPages).map((x, i) => i + 1);
				this.pagination.size = this.jobEmployersCount;
				this.pager = this.pageService.getPager(this.jobEmployersCount, this.filters.page_no, this.pagination.count);
				this.pageIndex = (this.pager.currentPage - 1) * this.filters.limit;
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.loadingMsg = 'Employer Not Found';
			// this.renderer.removeClass(this.document.body, 'show-spinner');
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
			// console.log("error", error);
		});
	}

	setPage(pagenumber: number) {
		this.pagination.page = pagenumber;
		this.filters.page_no = pagenumber;
		this.filters.offset = this.page * (pagenumber - 1);
		this.pager = this.pageService.getPager(this.pagination.size, this.pagination.page, this.pagination.count);
		this.getEmployers(this.filters,);
	}

	BlockEmp(text, empId) {
		this.blockText = (text == 'block') ? 'block' : 'unblock';
		this.userId = empId;
	}

	blockCustomerConfirm(type) {
		let block = (this.blockText == 'block') ? true : false;
		let blockMsg = (this.blockText == 'block') ? 'Blocked' : 'Unblocked';
		let data = {
			isBlocked: block,
			userId: this.userId
		}
		this.employersService.blockEmployer(data).subscribe((employee: any) => {
			// console.log("employee", employee);
			if (employee && employee.statusCode == 200) {
				this.toastr.success(`Employer ${blockMsg} Successfully`);
				this.getEmployers(this.filters);

			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.toastr.error(error.error.message);
		});

	}
}


