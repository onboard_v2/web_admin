import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication/authentication';
import { CertificatesService } from '../../services/certificates/certificates.service';
import { PageService } from '../../services/pagination/page.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.css']
})
export class CertificatesComponent implements OnInit {

	certificates = [];
	certificatesCount = 0;
	loadingMsg = 'Loading Certificates.';
	pageIndex;
	totalPages = [];
	pager: any = {};
	config: any;
	page = 30;
	offset = 0;
	pagination = {
		count: this.page,
		page: 1,
		pages: 0,
		size: 0
	};
	filters = {
		page_no: 1,
		limit: this.page,
		offset: this.offset
	}
	certForm: FormGroup;
	btnName = 'Submit';
	submitted = false;
	certificateId = '';
	deleteBtnName = 'Delete';
	editBtnName = 'Update';
	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private certificatesService: CertificatesService,
		private pageService: PageService,
		private toastr: ToastrService,
		private route: ActivatedRoute,
		private formBuilder: FormBuilder,
		@Inject(DOCUMENT) private document: Document,) {
		this.config = {
			currentPage: 1,
			itemsPerPage: this.page
		};
		this.pagination = {
			count: this.page,
			page: 1,
			pages: 0,
			size: 0
		};
	}

	ngOnInit() {
		this.certForm = this.formBuilder.group({
			cert_name: ['', Validators.required]
		});
		this.getCertificates(this.filters);
	}


	getCertificates(filterData) {

		this.certificatesService.getCertificates(filterData).subscribe((certificates: any) => {
			this.loadingMsg = 'Certificates Not Found';
			console.log("certificates", certificates);
			if (certificates && certificates.statusCode == 200) {
				this.certificates = certificates.data.categoryData;
				this.certificatesCount = certificates.data.count;
				let tPages = Math.ceil(this.certificatesCount / this.config.itemsPerPage);
				this.totalPages = Array(tPages).fill(tPages).map((x, i) => i + 1);
				this.pagination.size = this.certificatesCount;
				this.pager = this.pageService.getPager(this.certificatesCount, this.filters.page_no, this.pagination.count);
				this.pageIndex = (this.pager.currentPage - 1) * this.filters.limit;
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.loadingMsg = 'Certificates Not Found';
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
		});
	}

	setPage(pagenumber: number) {
		this.pagination.page = pagenumber;
		this.filters.page_no = pagenumber;
		this.filters.offset = this.page * (pagenumber - 1);
		this.pager = this.pageService.getPager(this.pagination.size, this.pagination.page, this.pagination.count);
		this.getCertificates(this.filters);
	}

	get f() { return this.certForm.controls; }

	onSubmit() {

		this.submitted = true;
		if (this.certForm.invalid) {
			return;
		}
		this.btnName = 'Loading...';

		let data = {
			categoryName: "CERTIFICATE",
			description: this.f.cert_name.value
		};

		this.certificatesService.addCertificate(data).subscribe((certData: any) => {
				
				if (certData && certData.statusCode === 200) {
					this.toastr.success('Certificate Added Successfully.', 'Success!');
					this.getCertificates(this.filters);
					$('#addModal').modal('hide'); 
					this.certForm.patchValue({
						'cert_name': ''
					});
					this.submitted =false;
					this.btnName = 'Submit';
					
				} else {
					this.btnName = 'Submit';
					this.toastr.error('Error in Api');	
				}
			}, error => {
				this.toastr.error(error.error.message);
				this.btnName = 'Submit';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}

	editCertificate(certId, name){
		this.certificateId = certId;
		this.certForm.patchValue({
			cert_name : name
		})
	}
	onUpdate() {

		this.submitted = true;
		if (this.certForm.invalid) {
			return;
		}
		this.editBtnName = 'Updating...';
		console.log("this.f.cert_name.value", this.f.cert_name.value);
		let data = {
			categoryId: this.certificateId,
			description: this.f.cert_name.value
		};

		this.certificatesService.updateCertificate(data).subscribe((certData: any) => {
				
				if (certData && certData.statusCode === 200) {
					this.toastr.success('Certificate Updated Successfully.', 'Success!');
					this.getCertificates(this.filters);
					$('#editModal').modal('hide'); 
					this.certForm.patchValue({
						'cert_name': ''
					});
					this.submitted =false;
					this.editBtnName = 'Update';
					
				} else {
					this.editBtnName = 'Update';
					this.toastr.error('Error in Api');	
				}
			}, error => {
				this.toastr.error(error.error.message);
				this.editBtnName = 'Update';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}

	deleteCertificate(certId){
		console.log("certId", certId);
		this.certificateId = certId;
	}

	deleteCertificateConfirm(){
		let data = {
			"categoryId" : this.certificateId
		}
		this.deleteBtnName = 'Deleting...'
		this.certificatesService.deleteCertificate(data).subscribe((certData: any) => {
				console.log("certData", certData);
				if (certData && certData.statusCode === 200) {
					this.toastr.success('Certificate Deleted Successfully.', 'Success!');
					this.getCertificates(this.filters);
					$('#deleteModal').modal('hide'); 
					this.deleteBtnName = 'Delete';
				} else {
					this.deleteBtnName = 'Delete';
					this.toastr.error('Error in Api');	
				}
			}, error => {
				this.deleteBtnName = 'Delete';
				this.toastr.error(error.error.message);
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}

}

