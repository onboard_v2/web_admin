import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication/authentication';
import { EmailValidator, PasswordValidator } from '../../validators';
import { ToastrService } from 'ngx-toastr';
@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {


	signUpForm: FormGroup;
	uploadForm: FormGroup;
	loading = false;
	submitted = false;
	error = '';
	signupBtn = 'Sign up';
	isUploadImg = false;
	imgUrl: any;
	profileUrl = '';
	token = '';
	uploadLogoText = 'Upload Logo';
	constructor(private router: Router, private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private toastrService: ToastrService, @Inject(DOCUMENT) private document: Document,
		private renderer: Renderer2) { }

	ngOnInit() {
		this.renderer.addClass(this.document.body, 'background');
		this.renderer.addClass(this.document.body, 'no-footer');
		this.signUpForm = this.formBuilder.group({
			name: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
			email: ['', Validators.compose([Validators.required, EmailValidator.email])],
			password: ['', Validators.compose([Validators.required, PasswordValidator.password])],
			// abnNumber: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
			abnNumber: ['', '']
		});
		this.uploadForm = this.formBuilder.group({
			image: ['']
		});
	}

	loginBtn() {
		this.router.navigateByUrl('/login');
	}

	get f() { return this.signUpForm.controls; }

	onSubmit() {
		this.submitted = true;
		console.log("this.signUpForm", this.signUpForm);
		// stop here if form is invalid
		if (this.signUpForm.invalid) {
			return;
		}
		this.signupBtn = 'Loading...';
		this.loading = true;

		let fData: FormData = new FormData;
		fData.append("name", this.f.name.value);
		fData.append("email", this.f.email.value);
		fData.append("password", this.f.password.value);
		if (this.f.abnNumber.value) {
			fData.append("abnNumber", this.f.abnNumber.value);
		}
		fData.append("deviceType", 'WEB');
		this.authenticationService.signUp(fData)
			.pipe(first())
			.subscribe((data: any) => {

				if (data.statusCode === 200) {
					this.toastrService.success('We have sent a verification mail to your registered email id. Click on the link to verify your email Id.');
					this.isUploadImg = true;
					// localStorage.setItem('token', JSON.stringify(data.data.token));
					// localStorage.setItem('userId', JSON.stringify(data.data.user._id));
					// this.router.navigate(['/login']);
				} else {
					this.loading = false;
					this.signupBtn = 'Sign up';
					// this.error = data.message;;
					this.toastrService.error(data.message);
					console.log("Error", data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				// this.error = error.message;
				this.loading = false;
				this.signupBtn = 'Sign up';
			});
	}


	skip() {
		this.router.navigate(['/new-job-post']);
	}
	imgUpload() {
		document.getElementById('my_file').click();

	}


	onSelectFile(event) {
		event.preventDefault();
		this.uploadLogoText = 'Change Logo';
		if (event.target.files && event.target.files[0]) {

			this.uploadForm.get('image').setValue(event.target.files[0]);
			let fileReader = new FileReader();
			fileReader.readAsDataURL(event.target.files[0]); // read file as data url

			fileReader.onload = (evt: Event) => { // called once readAsDataURL is completed

				this.imgUrl = fileReader.result;
				console.log("this.imgUrl", this.imgUrl);
			};
		}
	}

	// completeSignUp() {
	// 	console.log("completeSignUp");
	// 	let imgData: FormData = new FormData;
	// 	imgData.append('profilePic', this.uploadForm.get('image').value);

	// 	this.createJobService.editProfileImg(imgData)
	// 		.pipe(first())
	// 		.subscribe((proResp: any) => {
	// 			console.log('proResp', proResp);
	// 			if (proResp.statusCode === 200) {
	// 				// let currentUser = localStorage.getItem("currentUser");
	// 				// let localData = JSON.parse(currentUser);
	// 				// let updatedUser = {"token" : localData.token, "user" : proResp.data.user };
	// 				// localStorage.setItem('currentUser', JSON.stringify(updatedUser));
	// 				this.toastrService.success('Profile image uploaded succesfully.');
	// 				this.skip();
	// 			} else {
	// 				this.toastrService.error(proResp.message);
	// 			}
	// 		}, error => {
	// 			console.log("error", error);
	// 			this.toastrService.error(error.error.message);
	// 		});
	// }
}
