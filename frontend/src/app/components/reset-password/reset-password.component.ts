import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication/authentication';
import { UserProfileService } from '../../services/user-profile/user-profile.service';
import { EmailValidator, PasswordValidator } from '../../validators';
import { ToastrService } from 'ngx-toastr';
import { MustMatch } from '../../validators/password-match.validator';
@Component({
	selector: 'app-reset-password',
	templateUrl: './reset-password.component.html',
	styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

	resetForm: FormGroup;
	submitBtn = 'Reset password';
	submitted = false;
	passwordToken = '';
	tokenRole = '';
	constructor(private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private toastrService: ToastrService, @Inject(DOCUMENT) private document: Document,
		private renderer: Renderer2, private userProfileService: UserProfileService) { }

	ngOnInit() {

		this.passwordToken = this.route.snapshot.params.passwordToken;
		console.log("this.passwordToken", this.passwordToken);

		this.route.queryParams.subscribe(params => {
	      this.tokenRole = params.role;
	      console.log("this.tokenRole", this.tokenRole);
	    });

		this.resetPasswordValidator(this.passwordToken);
		this.renderer.addClass(this.document.body, 'background');
		this.renderer.addClass(this.document.body, 'no-footer');
		this.resetForm = this.formBuilder.group({
			password: ['', Validators.compose([Validators.required, PasswordValidator.password])],
			confirmPassword: ['', Validators.required]
		}, {
			validator: MustMatch('password', 'confirmPassword')
		});

	}

	signupBtn() {
		this.router.navigateByUrl('/register');
	}

	signinBtn() {
		this.router.navigateByUrl('/login');
	}
	get f() { return this.resetForm.controls; }

	onSubmit() {
		this.submitted = true;
		if (this.resetForm.invalid) {
			return;
		}
		this.submitBtn = 'Updating...';
		// console.log("this.f.password.value", this.f.password.value);
		let fData: FormData = new FormData;
		fData.append("newPassword", this.f.password.value);
		fData.append("passwordResetToken", this.passwordToken);
		console.log("fData", fData);
		this.userProfileService.resetPassword(fData)
			.pipe(first())
			.subscribe((data: any) => {
				this.submitBtn = 'Reset Password';
				if (data.statusCode === 200) {
					
					if(this.tokenRole == 'employer'){
						this.toastrService.success('Your password has been reset.');
						setTimeout(() => {
							this.router.navigate(['/login']);
						}, 2000);
					} else{
						this.toastrService.success('Your password has been reset. Please try to login again.');
						this.resetForm.reset();
						this.resetForm.controls.password.setErrors(null);
						this.resetForm.controls.confirmPassword.setErrors(null);
					}
				} else {
					this.toastrService.error(data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				this.submitBtn = 'Reset Password';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}


	resetPasswordValidator(passwordToken) {
		// let Data: FormData = new FormData;
		// Data.append("passwordResetToken", passwordToken);
		let data = {
			passwordResetToken: passwordToken
		}
		this.userProfileService.resetPasswordValidator(data)
			.pipe(first())
			.subscribe((data: any) => {
				console.log("data", data);
				// this.submitBtn = 'Reset Password';
				// if (data.statusCode === 200) {
				// 	this.toastrService.success('New Password Updated.');
				// 	// setTimeout(() => {
				// 	this.router.navigate(['/login']);
				// 	// }, 5000);
				// } else {
				// 	this.toastrService.error(data.message);
				// }
			}, error => {
				this.toastrService.error(error.error.message);
			});


	}
}
