import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication';
import { JobsService } from '../../../services/jobs/jobs.service';
import { PageService } from '../../../services/pagination/page.service';
import { ToastrService } from 'ngx-toastr';
@Component({
	selector: 'app-job-employees',
	templateUrl: './job-employees.component.html',
	styleUrls: ['./job-employees.component.css']
})
export class JobEmployeesComponent implements OnInit {

	jobsEmp = [];
	jobsEmpCount = 0;
	loadingMsg = 'Loading Employees.';
	totalPages = [];
	pager: any = {};
	pageIndex;
	config: any;
	page = 15;
	offset = 0;
	pagination = {
		count: this.page,
		page: 1,
		pages: 0,
		size: 0
	};
	filters = {
		page_no: 1,
		limit: this.page,
		offset: this.offset,
		jobId: ''
	}

	constructor(private router: Router,
		private authenticationService: AuthenticationService,
		private jobsService: JobsService,
		private pageService: PageService,
		private route: ActivatedRoute,
		private toastr: ToastrService) {
		this.config = {
			currentPage: 1,
			itemsPerPage: this.page
		};
		this.pagination = {
			count: this.page,
			page: 1,
			pages: 0,
			size: 0
		};
	}

	ngOnInit() {
		this.filters.jobId = this.route.snapshot.url[3].path;
		this.getJobEmployees(this.filters);
	}

	getJobEmployees(filterData) {

		this.jobsService.getJobEmployees(filterData).subscribe((results: any) => {

			this.loadingMsg = 'Employee Not Found';
			if (results && results.statusCode == 200) {
				this.jobsEmp = results.data.users;
				this.jobsEmpCount = results.data.count;
				let tPages = Math.ceil(this.jobsEmpCount / this.config.itemsPerPage);
				this.totalPages = Array(tPages).fill(tPages).map((x, i) => i + 1);
				this.pagination.size = this.jobsEmpCount;
				this.pager = this.pageService.getPager(this.jobsEmpCount, this.filters.page_no, this.pagination.count);
				this.pageIndex = (this.pager.currentPage - 1) * this.filters.limit;
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.loadingMsg = 'Employee Not Found';
			// this.renderer.removeClass(this.document.body, 'show-spinner');
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
			// console.log("error", error);
		});
	}

	setPage(pagenumber: number) {
		this.pagination.page = pagenumber;
		this.filters.page_no = pagenumber;
		this.filters.offset = this.page * (pagenumber - 1);
		this.pager = this.pageService.getPager(this.pagination.size, this.pagination.page, this.pagination.count);
		this.getJobEmployees(this.filters,);
	}


}

