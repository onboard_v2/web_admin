import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobEmployeesComponent } from './job-employees.component';

describe('JobEmployeesComponent', () => {
  let component: JobEmployeesComponent;
  let fixture: ComponentFixture<JobEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobEmployeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
