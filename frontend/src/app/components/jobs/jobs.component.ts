import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication/authentication';
import { JobsService } from '../../services/jobs/jobs.service';
import { PageService } from '../../services/pagination/page.service';
import { ToastrService } from 'ngx-toastr';
@Component({
	selector: 'app-jobs',
	templateUrl: './jobs.component.html',
	styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

	jobs = [];
	jobsCount = 0;
	jobType = 'posted'
	loadingMsg = 'Loading Jobs.';
	searchJob = '';
	totalPages = [];
	pager: any = {};
	config: any;
	page = 15;
	offset = 0;
	pageIndex;
	pagination = {
		count: this.page,
		page: 1,
		pages: 0,
		size: 0
	};
	filters = {
		page_no: 1,
		limit: this.page,
		offset: this.offset,
		type: 'POSTED',
		searchJobTitle : ''
	}

	constructor(private router: Router,
		private authenticationService: AuthenticationService,
		private jobsService: JobsService,
		private pageService: PageService,
		private route: ActivatedRoute,
		private toastr: ToastrService) {
		this.config = {
			currentPage: 1,
			itemsPerPage: this.page
		};
		this.pagination = {
			count: this.page,
			page: 1,
			pages: 0,
			size: 0
		};
	}

	ngOnInit() {

		if (this.route.snapshot.url[1].path == 'past') {
			this.filters.type = 'PAST';
			this.jobType = 'past';
		} else if (this.route.snapshot.url[1].path == 'upcoming') {
			this.filters.type = 'UPCOMING';
			this.jobType = 'upcoming';
		} else if (this.route.snapshot.url[1].path == 'ongoing') {
			this.filters.type = 'ONGOING';
			this.jobType = 'ongoing';
		} else {
			this.filters.type = 'POSTED';
			this.jobType = 'posted';
		}
		this.getJobs(this.filters);

	}

	getJobs(filterData) {

		this.jobsService.getJobs(filterData).subscribe((results: any) => {

			this.loadingMsg = 'Jobs Not Found';
			if (results && results.statusCode == 200) {
				this.jobs = results.data.jobs;
				this.jobsCount = results.data.count;
				let tPages = Math.ceil(this.jobsCount / this.config.itemsPerPage);
				this.totalPages = Array(tPages).fill(tPages).map((x, i) => i + 1);
				this.pagination.size = this.jobsCount;
				this.pager = this.pageService.getPager(this.jobsCount, this.filters.page_no, this.pagination.count);
				this.pageIndex = (this.pager.currentPage - 1) * this.filters.limit;
				// this.pageIndex = (this.page)*(this.pager.currentPage - 1)
			} else {
				this.toastr.error('Error in Api');
			}
		}, error => {
			this.loadingMsg = 'Jobs Not Found';
			// this.renderer.removeClass(this.document.body, 'show-spinner');
			this.toastr.error(error.error.message);
			if (error.error.error == "Unauthorized") {
				this.authenticationService.logout();
			}
			// console.log("error", error);
		});
	}

	setPage(pagenumber: number) {
		this.pagination.page = pagenumber;
		this.filters.page_no = pagenumber;
		this.filters.offset = this.page * (pagenumber - 1);
		this.pager = this.pageService.getPager(this.pagination.size, this.pagination.page, this.pagination.count);
		this.getJobs(this.filters);
	}

	searchJobTitle(searchVal) {
		
	    if (searchVal.length > 0) {
	      this.filters.searchJobTitle = searchVal;
	    } else {
	      
	      this.filters.searchJobTitle = '';
	    }
    	this.getJobs(this.filters);
  }


}
