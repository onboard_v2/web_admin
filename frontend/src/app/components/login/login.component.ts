import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication/authentication';
import { ToastrService } from 'ngx-toastr';
import { EmailValidator, PasswordValidator } from '../../validators';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	loginForm: FormGroup;
	loading = false;
	submitted = false;
	returnUrl: string;
	error = '';
	loginBtn = 'Sign in';
	userFirstname = '';
	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		@Inject(DOCUMENT) private document: Document,
		private renderer: Renderer2,
		private toastrService: ToastrService
	) {
		// redirect to home if already logged in
		if (this.authenticationService.currentUserValue) {
			this.router.navigate(['/dashboard']);
		}
	}

	ngOnInit() {
		// this.renderer.removeClass(this.document.body, 'embedded-body');
		this.renderer.addClass(this.document.body, 'background');
		this.renderer.addClass(this.document.body, 'no-footer');
		this.loginForm = this.formBuilder.group({
			email: ['', Validators.compose([Validators.required, EmailValidator.email])],
			password: ['', Validators.required]
			// password: ['', Validators.compose([Validators.required, PasswordValidator.password])],
		});
		// get return url from route parameters or default to '/'
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
	}

	// convenience getter for easy access to form fields
	get f() { return this.loginForm.controls; }

	onSubmit() {
		this.submitted = true;

		// stop here if form is invalid
		if (this.loginForm.invalid) {
			return;
		}
		this.loginBtn = 'Loading...';
		this.loading = true;
		let data = {
			email: this.f.email.value,
			password: this.f.password.value,
			deviceType: 'WEB',
			role: "admin"
		};
		this.authenticationService.login(data)
			.pipe(first())
			.subscribe(data => {
				console.log("data", data);
				if (data.statusCode === 200) {
					this.toastrService.success('Login Successfully.', 'Success!');
					this.router.navigate([this.returnUrl]);
				} else {
					this.loading = false;
					this.loginBtn = 'Sign in';
					this.error = data.message;;
					console.log("Error", data.message);
				}
			}, error => {
				console.log("error", error);
				this.error = error.message;
				this.toastrService.error(error.error.message);
				this.loading = false;
				this.loginBtn = 'Sign in';
			});
	}


	signupBtn() {
		this.router.navigateByUrl('/register');
	}

}
