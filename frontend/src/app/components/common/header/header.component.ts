import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  username = '';
  profilePic = '';
  email = '';
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    console.log("currentUser", currentUser);
    this.username = currentUser ? currentUser.user.name : '';
    this.email = currentUser ? currentUser.user.email : '';
    this.profilePic = currentUser ? currentUser.user.profilePic : '';
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}

