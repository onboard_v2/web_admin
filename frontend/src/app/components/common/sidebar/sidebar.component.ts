import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication/authentication';
@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

	loading = false;
	username = '';
	transform = true;
	constructor(private authenticationService: AuthenticationService) { }

	ngOnInit() {
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		this.username = currentUser ? currentUser.user.name : '';
	}

	logout() {
		this.loading = true;
		this.authenticationService.logout();
	}

	showHideMenu() {
		let element = document.getElementById("app-container");
		// element.classList.toggle("main-show-temporary");
		element.classList.toggle("open-menu");
	}

	showMenus() {
		this.transform = false;
		console.log("showMenus");
	}

}



