import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication';
@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent implements OnInit {

  loading = false;
  username = '';
  profilePic = '';
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.username = currentUser ? currentUser.user.name : '';
    this.profilePic = currentUser ? currentUser.user.profilePic : '';
  }

  logout() {
    console.log("logout");
    this.loading = true;
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  mobileSideMenu() {
    let element = document.getElementById("app-container");
    element.classList.toggle("open-mobile-menu");
  }


}
