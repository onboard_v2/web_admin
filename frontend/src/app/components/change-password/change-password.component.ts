import { Component, Inject, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication/authentication';
import { UserProfileService } from '../../services/user-profile/user-profile.service';
import { EmailValidator, PasswordValidator } from '../../validators';
import { ToastrService } from 'ngx-toastr';
import { MustMatch } from '../../validators/password-match.validator';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  	
  changePasswordForm: FormGroup;
  submitBtn = 'Save';
  submitted = false;
  profilePic = '';
  name = '';
  constructor(private router: Router, private formBuilder: FormBuilder, private authenticationService :AuthenticationService,private toastrService: ToastrService,@Inject(DOCUMENT) private document: Document,
    	private renderer: Renderer2, private userProfileService: UserProfileService) { }

  ngOnInit() {
  	let currentUser = JSON.parse(localStorage.getItem('currentUser'));
	this.name = currentUser ? currentUser.user.name : '';
	this.profilePic = currentUser ? currentUser.user.profilePic : '';
  	this.changePasswordForm = this.formBuilder.group({
            password: ['', Validators.compose([Validators.required, PasswordValidator.password])],
            confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword')
        });

  }

  get f() { return this.changePasswordForm.controls; }

  onSubmit() {
		this.submitted = true;
		this.submitBtn = 'Updating...';
		
		if (this.changePasswordForm.invalid) {
			return;
		}
		// console.log("this.f.password.value", this.f.password.value);
		let fData: FormData = new FormData;
		fData.append("newPassword", this.f.password.value);
		console.log("fData", fData);
		this.userProfileService.updatePassword(fData)
			.pipe(first())
			.subscribe((data: any) => {
				this.submitBtn = 'Save';
				if (data.statusCode === 200) {
					this.toastrService.success('Password Updated.');
				} else {
					this.toastrService.error(data.message);
				}
			}, error => {
				this.toastrService.error(error.error.message);
				this.submitBtn = 'Save';
				if (error.error.error == "Unauthorized") {
					this.authenticationService.logout();
				}
			});
	}


}
