const express=require("express"),
path = require('path');
app = express();

app.use(express.static(path.join(__dirname, './dist/frontend')));
app.use('/*', express.static(path.join(__dirname, './dist/frontend')));

app.listen(3008, ()=> {
   console.log(`Server listening 3008`);
});
